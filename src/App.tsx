import React from "react";

import styles from "./assets/css/app-base.module.css";
import Dashboard from "./containers/Dashboard";
import Header from "./components/Header";
import "antd/dist/antd.css";

function App() {
  return (
    <div className={`App ${styles.app}`}>
      <Header />
      <Dashboard />
    </div>
  );
}

export default App;
