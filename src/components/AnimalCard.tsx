import React from "react";
import { Animals } from "../models";
import { Card } from "antd";

interface AnimalCardProps {
  animalsList: Animals;
  onLoadUsersPerAnimal: (a: string, pag: string) => void;
  setCurrentAnimal: (a: string, pag: string) => void;
}
const AnimalCard = ({
  animalsList,
  onLoadUsersPerAnimal,
  setCurrentAnimal,
}: AnimalCardProps) => {
  const gridStyle: React.CSSProperties = {
    width: "25%",
    textAlign: "center",
  };

  return (
    <Card title="All animals">
      {animalsList.map((a) => {
        return (
          typeof a === "string" && (
            <Card.Grid style={gridStyle} key={`available-animal-${a}`}>
              <div
                className="animalsList"
                onClick={() => {
                  onLoadUsersPerAnimal(a, "10");
                  setCurrentAnimal(a, "10");
                }}
              >
                {a}
              </div>
            </Card.Grid>
          )
        );
      })}
    </Card>
  );
};
export default AnimalCard;
