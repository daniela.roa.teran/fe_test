import React from "react";
import styles from "../assets/css/header.module.css";

const Header = () => {
  return (
    <div className={styles.container}>
      <h1 className={styles.title}>
        WE
        <br />
        LOVE
        <br />
        ANIMALS
      </h1>
      <img src="/images/paw.png" alt="paw" />
    </div>
  );
};
export default Header;
