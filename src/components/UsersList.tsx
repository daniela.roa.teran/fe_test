import React from "react";
import { UserList } from "../models";

import { List, Button, Avatar } from "antd";
import { StarTwoTone } from "@ant-design/icons";

interface UsersListProps {
  userPerAnimalList: UserList;
  loading: boolean;
  showMore: boolean;
  currentAnimal: string;
  onLoadUsersPerAnimal: (currentAnimal: string, pag: string) => void;
  onRemoveUser: (id: string) => void;
}

const UsersList = ({
  userPerAnimalList,
  loading,
  showMore,
  currentAnimal,
  onLoadUsersPerAnimal,
  onRemoveUser,
}: UsersListProps) => {
  return (
    <List itemLayout="horizontal" loading={loading} size="small">
      {userPerAnimalList.map((user, i) => {
        return (
          <List.Item
            key={`top-user-${user}-${i}`}
            actions={[
              <span
                key="list-loadmore-edit"
                onClick={() => onRemoveUser(user.id)}
                style={{ cursor: "pointer" }}
              >
                remove
              </span>,
            ]}
          >
            <List.Item.Meta
              avatar={
                <Avatar
                  style={{ color: "#fafafa", backgroundColor: "#0097A7" }}
                >
                  {user.name.given.slice(0, 1).toUpperCase()}
                  {user.name.surname.slice(0, 1).toUpperCase()}
                </Avatar>
              }
              title={
                <>
                  {user.name.given} {user.name.surname}
                </>
              }
              description={
                <>
                  {user.points} <StarTwoTone twoToneColor="#faad14" />
                </>
              }
            />
          </List.Item>
        );
      })}
      {!loading && showMore ? (
        <div
          style={{
            textAlign: "center",
            marginTop: 12,
            height: 32,
            lineHeight: "32px",
          }}
        >
          <Button onClick={() => onLoadUsersPerAnimal(currentAnimal, "25")}>
            Show more
          </Button>
        </div>
      ) : null}
    </List>
  );
};
export default UsersList;
