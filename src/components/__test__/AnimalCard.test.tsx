import { shallow } from "enzyme";
import { AnimalCard } from "../";

const onLoadUsersPerAnimal = jest.fn();
const setCurrentAnimal = jest.fn();
const animalsList = ["cat", "dog", "chihuahua"];
const defaultProps = {
  animalsList,
  onLoadUsersPerAnimal,
  setCurrentAnimal,
};

describe("Animals card component", () => {
  const wrapper = shallow(<AnimalCard {...defaultProps} />);
  it("title All animals must exists", () => {
    expect(wrapper.prop("title")).toEqual("All animals");
  });
  it("renders cards with available animals", () => {
    expect(wrapper.children().length).toEqual(animalsList.length);
  });
  it("on click children card", () => {
    expect(wrapper.children().at(0).find("div").prop("onClick")).toBeDefined();
    wrapper.children().at(0).find("div").prop("onClick")();
    expect(onLoadUsersPerAnimal).toHaveBeenCalledTimes(1);
    expect(setCurrentAnimal).toHaveBeenCalledTimes(1);
  });
});
