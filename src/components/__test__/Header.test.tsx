import { shallow } from "enzyme";
import Header from "../Header";

describe("Header component", () => {
  const wrapper = shallow(<Header />);
  it("title we love animals must exists", () => {
    expect(wrapper.find("h1").exists()).toEqual(true);
    expect(wrapper.find("h1").text()).toEqual("WELOVEANIMALS");
  });

  it("image props called correctly", () => {
    expect(wrapper.find("img").prop("alt")).toEqual("paw");
    expect(wrapper.find("img").prop("src")).toEqual("/images/paw.png");
  });
});
