import { shallow } from "enzyme";
import { UsersList } from "../";

import { List, Button } from "antd";

const userPerAnimalList = [
  {
    id: "5fbfe211bc2f84035ccfcabf",
    name: {
      given: "Clemons",
      surname: "Johns",
    },
    points: 93,
    animals: [
      "panda",
      "lion",
      "horse",
      "cat",
      "monkey",
      "bear",
      "jaguar",
      "kangaroo",
      "elephant",
      "dog",
      "koala",
      "gorilla",
    ],
    isActive: true,
    age: 23,
  },
];
const loading = false;
const showMore = false;
const currentAnimal = "Cat";
const onLoadUsersPerAnimal = jest.fn();
const onRemoveUser = jest.fn();

const defaultProps = {
  userPerAnimalList,
  loading,
  showMore,
  currentAnimal,
  onLoadUsersPerAnimal,
  onRemoveUser,
};
describe("User List component", () => {
  const wrapper = shallow(<UsersList {...defaultProps} />);
  it("List component exists", () => {
    expect(wrapper.find(List)).toBeTruthy();
  });
  it("List component has children", () => {
    expect(wrapper.find(List).children().length).toEqual(
      userPerAnimalList.length
    );
  });

  it("on click list item remove option", () => {
    wrapper
      .find(List.Item)
      .prop("actions")
      ?.forEach((sp) => {
        expect(shallow(sp).find("span").prop("onClick")).toBeDefined();
        shallow(sp).find("span").prop("onClick")();
        expect(onRemoveUser).toBeCalledTimes(1);
      });
  });

  it("on click show more", () => {
    const newWrapper = shallow(
      <UsersList {...defaultProps} loading={false} showMore={true} />
    );
    expect(newWrapper.find(List).find(Button).text()).toEqual("Show more");
    expect(newWrapper.find(List).find(Button).prop("onClick")).toBeDefined();
    newWrapper.find(List).find(Button).prop("onClick")();
    expect(onLoadUsersPerAnimal).toBeCalledTimes(1);
  });
});
