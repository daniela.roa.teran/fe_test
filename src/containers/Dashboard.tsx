import React from "react";
import { useState, useMemo, useEffect } from "react";
import styles from "../assets/css/dashboard.module.css";
import { Animals, UserList } from "../models";
import { AnimalCard, UsersList } from "../components";
import { AnimalService, UserService } from "../fake-backend/services";

import { Input, Tooltip } from "antd";
import { InfoCircleOutlined } from "@ant-design/icons";

const Dashboard = () => {
  const [animalsList, setAnimalsList] = useState<Animals>([]);
  const [userPerAnimalList, setUserPerAnimalList] = useState<UserList>(
    [] as UserList
  );
  const [handleLoading, setHandleLoading] = useState({
    loading: false,
    showMore: false,
  });
  const [currentAnimal, setCurrentAnimal] = useState({ animal: "", pag: "" });

  const { Search } = Input;
  const animalService = useMemo(() => new AnimalService(), []);
  const userService = useMemo(() => new UserService(), []);
  console.log(useState);
  useEffect(() => {
    animalService.getAvailableAnimals().then((animals) => {
      setAnimalsList(animals);
    });
  }, [animalService]);

  const onSearch = (search: string) => {
    if (search) {
      let filteredAnimals = animalsList.filter((a) => {
        return a.includes(search);
      });
      setAnimalsList(filteredAnimals);
    } else {
      animalService.getAvailableAnimals().then((animals) => {
        setAnimalsList(animals);
      });
    }
  };
  const handleSelectedAnimal = (a: string, pag: string) => {
    setCurrentAnimal({ animal: a, pag: pag });
  };
  const onLoadUsersPerAnimal = (a: string, pag: string) => {
    setHandleLoading({ ...handleLoading, loading: true });
    animalService.getAnimalUserList(a, pag).then((users) => {
      setUserPerAnimalList(users);
      setHandleLoading({ loading: false, showMore: true });
    });
  };
  const onRemoveUser = (id: string) => {
    userService
      .removeUser(id, currentAnimal.animal, currentAnimal.pag)
      .then(() => {
        setUserPerAnimalList(
          userPerAnimalList.filter((user) => user.id !== id)
        );
      });
  };
  return (
    <div className={styles.container}>
      <div className={styles.searchBar}>
        <Search
          placeholder="Search your favorite animal"
          allowClear
          onSearch={onSearch}
          enterButton
        />
      </div>
      <AnimalCard
        animalsList={animalsList}
        onLoadUsersPerAnimal={onLoadUsersPerAnimal}
        setCurrentAnimal={handleSelectedAnimal}
      />
      <div className={styles.users}>
        <h3>
          <Tooltip title="Select an animal to see its top lovers">
            <InfoCircleOutlined />
          </Tooltip>{" "}
          Top animal lovers
        </h3>
        <UsersList
          userPerAnimalList={userPerAnimalList}
          loading={handleLoading.loading}
          showMore={handleLoading.showMore}
          onRemoveUser={onRemoveUser}
          currentAnimal={currentAnimal.animal}
          onLoadUsersPerAnimal={onLoadUsersPerAnimal}
        />
      </div>
    </div>
  );
};
export default Dashboard;
