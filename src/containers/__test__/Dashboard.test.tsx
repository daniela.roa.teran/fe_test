import React from "react";
import { shallow } from "enzyme";
import Dashboard from "../Dashboard";
import { AnimalCard } from "../../components";
import { AnimalService } from "../../fake-backend/services";

import { Input } from "antd";

describe("Dashboard container", () => {
  const wrapper = shallow(<Dashboard />);
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("search properties", () => {
    const search = wrapper.find(Input.Search);
    expect(search.exists()).toEqual(true);
    expect(search.prop("placeholder")).toEqual("Search your favorite animal");
  });

  it("on search function called with value", () => {
    const spy = jest
      .spyOn(AnimalService.prototype, "getAvailableAnimals")
      .mockResolvedValue(["cat"]);
    const search = wrapper.find(Input.Search);
    search.prop("onSearch")?.("cat");
    expect(spy).not.toHaveBeenCalled();
  });

  it("on search function called without value", () => {
    const spy = jest
      .spyOn(AnimalService.prototype, "getAvailableAnimals")
      .mockResolvedValue([""]);
    const search = wrapper.find(Input.Search);
    search.prop("onSearch")?.("");
    expect(spy).toHaveBeenCalledTimes(1);
  });

  it.skip("AnimalCard component props", () => {
    const mockUseState = jest.fn().mockReturnValue({ a: "cat", pag: "10" });
    jest.spyOn(React, "useState").mockReturnValue([{}, mockUseState]);
    const animalCard = wrapper.find(AnimalCard);
    expect(animalCard).toBeTruthy();
    expect(animalCard.prop("onLoadUsersPerAnimal")).toBeDefined();
    animalCard.prop("onLoadUsersPerAnimal")("cat", "10");
    expect(mockUseState).toHaveBeenCalledTimes(1);
  });
});
