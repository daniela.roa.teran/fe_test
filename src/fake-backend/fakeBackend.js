import { users } from "./fixtures/users";
var _ = require("lodash");

export function configureFakeBackend() {
  let realFetch = window.fetch;
  window.fetch = function (url, opts) {
    return new Promise((resolve, reject) => {
      setTimeout(handleRoute, 500);

      function handleRoute() {
        switch (true) {
          case url.endsWith("/users"):
            return getUsers();
          case url.endsWith("/animals"):
            return getAnimals();
          case url.includes("/animals/users"):
            return getTopUsers();
          case url.includes("/user/delete"):
            return removeUser();
          default:
            return realFetch(url, opts)
              .then((response) => resolve(response))
              .catch((error) => reject(error));
        }
      }

      function getActiveUsers() {
        return _.filter(users, ["isActive", true]);
      }

      function getUsers() {
        let activeUsers = getActiveUsers();
        resolve({
          ok: true,
          text: () => Promise.resolve(JSON.stringify(activeUsers)),
        });
      }
      async function removeUser() {
        const removedUserId = url.slice(
          url.indexOf("id=") + 3,
          url.indexOf("&animal=")
        );
        const activeUsers = await paginateUser();
        let newUserList = _.dropWhile(activeUsers, ["id", removedUserId]);
        resolve({
          ok: true,
          text: () => Promise.resolve(JSON.stringify(newUserList)),
        });
      }

      function getAnimals() {
        let activeUsers = getActiveUsers();
        let availableAnimals = [];
        activeUsers.map((u) => {
          availableAnimals = _.union(u.animals, availableAnimals);
          return availableAnimals;
        });
        resolve({
          ok: true,
          text: () => Promise.resolve(JSON.stringify(availableAnimals)),
        });
      }

      async function getAnimalsPerUser() {
        const searchedAnimal = url.slice(
          url.indexOf("animal=") + 7,
          url.indexOf("pag=") - 1
        );
        let activeUsers = getActiveUsers();
        let usersPerAnimal = [];
        await activeUsers.forEach((user) => {
          if (user.animals.includes(searchedAnimal)) usersPerAnimal.push(user);
        });
        return usersPerAnimal;
      }

      async function paginateUser() {
        const usersPerAnimal = await getAnimalsPerUser();
        const pag = url.slice(url.indexOf("pag=") + 4, url.length);
        let topUsersPerAnimal = _.orderBy(usersPerAnimal, "points", "desc");
        return topUsersPerAnimal.slice(0, parseInt(pag));
      }

      async function getTopUsers() {
        const topUsersPerAnimal = await paginateUser();
        resolve({
          ok: true,
          text: () => Promise.resolve(JSON.stringify(topUsersPerAnimal)),
        });
      }
    });
  };
}
