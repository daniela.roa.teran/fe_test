import { UserList, Animals } from "../../models";

export default class AnimalService {
  async getAvailableAnimals() {
    let animals: Animals = [];
    await fetch("/animals")
      .then((res) => res.text())
      .then((data) => {
        animals = JSON.parse(data);
      });
    return animals;
  }
  async getAnimalUserList(animal: string, pag: string) {
    let users: UserList = [];
    await fetch(`/animals/users/?animal=${animal}&pag=${pag}`)
      .then((res) => res.text())
      .then((data) => {
        users = JSON.parse(data);
      });
    return users;
  }
}
