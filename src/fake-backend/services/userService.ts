import { UserList } from "../../models";

export default class UserService {
  removeUser = async (id: string, animal: string, pag: string) => {
    let users: UserList = [];
    await fetch(`/user/delete/?id=${id}&animal=${animal}&pag=${pag}`)
      .then((res) => res.text())
      .then((data) => {
        users = JSON.parse(data);
      });
    return users;
  };
}
